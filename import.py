from os import listdir
from os.path import isfile, join
import csv
import MySQLdb
import logging

logging.basicConfig(level=logging.DEBUG)

csv_path = raw_input("Where are the CSV files? (/home/crespo/Desktop/csv-import/): ") or "/home/crespo/Desktop/csv-import/"
root_pwd = raw_input("What's your MySQL root password?: ")
db_name = raw_input("What's the MySQL database name? (csv_import): ") or "csv_import"

db = MySQLdb.connect(host="localhost", user="root", passwd=root_pwd, db=db_name, local_infile=1)

file_names = [f for f in listdir(csv_path) if isfile(join(csv_path, f))]
cur = db.cursor()

for file_name in file_names:
    table_name = file_name.lower().replace(" ", "_").replace(".csv", "").replace("-", "_")
    # print "Open {} as table {}".format(file_name, table_name)
    full_file_name = join(csv_path, file_name)
    with open (full_file_name) as csv_file:
        spamreader = csv.reader(csv_file, delimiter=',', quotechar='|')
        table_columns = []
        first_row = []
        for row in spamreader:
            first_row = row
            break
        table_columns.append({"name": "id", "type": "INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY"})
        unknown_column_count = 0
        for column in first_row:
            cname = column.lower().replace(" ", "_").replace("(", "_").replace(")", "_").replace(".", "_").replace("/", "_").replace("&", "and")
            if not cname:
                cname = "unknown_column_{}".format(unknown_column_count),
                unknown_column_count = unknown_column_count + 1

            table_columns.append(
                {"name": cname ,
                 "type": "VARCHAR(100)"})

        statement = "".join(["`{}` {}, ".format(c.get('name'), c.get('type')) for c in table_columns])

        statement = "create table `{}` ({})".format(table_name, statement)
        last_comma = statement.rfind(",")
        statement = statement[:last_comma] + statement[last_comma+1:] + " DEFAULT CHARSET=utf8"
        logging.debug("Create table: {}".format(statement))

        cur.execute(statement)

        load_col = "".join(["`{}`,".format(c.get('name')) for c in table_columns if c.get('name') != "id"])
        last_comma = load_col.rfind(",")
        load_col = load_col[:last_comma] + load_col[last_comma+1:]

        load = "LOAD DATA LOCAL INFILE '{}' INTO TABLE {} " \
               "FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\n' " \
               "IGNORE 1 LINES ({})".format(full_file_name, table_name, load_col)


        logging.debug("Load data: {}".format(load))

        cur.execute(load)

db.commit()
Certificates are stored in Google Drive for security purposes.

https://drive.google.com/a/woolworths.com.au/folderview?id=0B91Sl-GJH9EPMUFweVNpaFBFSFk&usp=sharing

DO NOT store .pem and .p12 files in the source control.

Copy the entire structure from google drive to this folder to be able to deploy to app engine:

dev/wfe-dev.pem
uat/wfe-uat.pem
prod/wfe-prod.pem

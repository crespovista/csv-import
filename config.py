import os

import hashlib
import os
import urllib2
import logging
import json

import httplib2
from google.appengine.api import memcache
from google.appengine.api import mail
from google.appengine.api import taskqueue
from google.appengine.api import users

from apiclient.discovery import build

from google.appengine.api import app_identity

APP_ROOT_PATH = os.path.dirname(__file__)


''' Common configuration '''

SERVER_SOFTWARE = os.environ.get('SERVER_SOFTWARE')

IS_LOCAL_DEV_SERVER = True if not SERVER_SOFTWARE or SERVER_SOFTWARE.startswith('Development') else False

if IS_LOCAL_DEV_SERVER:
    ACTIVITI_BASE_URL_PROD = 'http://localhost:8080/rest/service/'
    ACTIVITI_BASE_URL_UAT = 'http://localhost:8080/rest/service/'
    ACTIVITI_BASE_URL_DEV = 'http://localhost:8080/rest/service/'
else:
    ACTIVITI_BASE_URL_PROD = 'http://23.251.146.230:8080/activiti/service/'
    ACTIVITI_BASE_URL_UAT = 'http://146.148.45.229:8080/activiti/service/'
    ACTIVITI_BASE_URL_DEV = 'http://23.251.149.200:8080/activiti/service/'

ENVIRONMENTS = {'dev-wow-forms': {'environment': 'DEV',
                                  'client_email': '155395706545-2rqoeffuo10f6a912qi5i46l2pekqhfn@developer.gserviceaccount.com',
                                  'pem_file': 'certificates/dev-wow-forms/dev-wow-forms.pem',
                                  'app_email': 'DEV Automated Workflow <cloudfusion@woolworths.com.au>',
                                  'apps_admin': 'npreema@test.woolworths.com.au',
                                  'site_url': 'http://dev-wow-forms.appspot.com',
                                  'author_groups': [],
                                  'activiti_url': ACTIVITI_BASE_URL_DEV,
                                  'customer_id': 'my_customer'},

                'uat-wow-forms': {'environment': 'UAT',
                                  'client_email': '845234005278-dqk57s6a1kvci8kbmv1ejhtsm3v9rvnk@developer.gserviceaccount.com', #'755718561500@developer.gserviceaccount.com',
                                  'pem_file': 'certificates/uat-wow-forms/uat-wow-forms.pem',
                                  'apps_admin': 'admin.gads@woolworths.com.au',
                                  'app_email': 'UAT Automated Workflow <cloudfusion@woolworths.com.au>',
                                  'site_url': 'http://uat-wow-forms.appspot.com',
                                  'cloudsql_instance': 'uat-wow-forms:uat-forms-db',
                                  'author_groups': ['wow-forms-authors@woolworths.com.au'],
                                  'activiti_url': ACTIVITI_BASE_URL_UAT,
                                  'customer_id': 'C00phkzvz'},
}

running_app_id = app_identity.get_application_id() if SERVER_SOFTWARE else 'uat-wow-forms'

logging.debug("Running app ID {}".format(running_app_id))
service_acct_email = ENVIRONMENTS[running_app_id]['client_email']
service_acct_pem_file = ENVIRONMENTS[running_app_id]['pem_file']
app_email_sender = ENVIRONMENTS[running_app_id]['app_email']
activiti_url = ENVIRONMENTS[running_app_id]['activiti_url']
customer_id = ENVIRONMENTS[running_app_id]['customer_id']
current_environment = ENVIRONMENTS[running_app_id]['environment']
G_APPS_ADMIN = ENVIRONMENTS[running_app_id]['apps_admin']
G_APPS_SCOPES = ['https://www.googleapis.com/auth/admin.directory.user.readonly',
                 'https://www.googleapis.com/auth/admin.directory.group.readonly',
                 'https://www.googleapis.com/auth/admin.directory.group.member.readonly']
author_groups = ENVIRONMENTS[running_app_id]['author_groups']

is_production = True if ENVIRONMENTS[running_app_id]['environment'] == 'PROD' else False


'''GCS setup
If on any non-local environment, the same GCS bucket will be used.
'''
# gcs_retry_params = gcs.RetryParams(initial_delay=0.2,
#                                    max_delay=5.0,
#                                    backoff_factor=2,
#                                    max_retry_period=15)
# gcs.set_default_retry_params(gcs_retry_params)
GCS_BUCKET = '/wowapp00001/'

'''Current development server does not seem to work with GCS
http://stackoverflow.com/questions/19727344/google-cloud-storage-client-not-working-on-dev-appserver
'''
DEV_SERVER_GCS_LOCATION = 'gcs'

#Authorisation
CACHE_G_APPS_USER_ID_KEY_PREFIX = '__gae_userid_'
CACHE_G_APPS_USER_ID_EXPIRY_MINS = 20



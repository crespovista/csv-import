import webapp2
import os
import re
import config
import httplib2
from httplib import HTTPException
from google.appengine.api import memcache
import copy
import logging

from apiclient import discovery
import config
import MySQLdb
import gdata.spreadsheet
import gdata.spreadsheet.service
FILE_ID = "1M5KP7IYSSFSRJcvSsV-oAFqHPB6nplmyIYX9JiwF8uE"
WORKSHEET_ID = "od6"
SPREADSHEET_SHARED = ["cwang@woolworths.com.au"]

db = MySQLdb.connect(host="localhost", user="root", passwd="wp1986dl", db="csv_import", local_infile=1)
cur = db.cursor()
class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.write('Hello, World!')

class GoogleService(webapp2.RequestHandler):
    def get(self):
        link = None

        self.response.headers['Content-Type'] = 'text/plain'
        payloads = self.get_payload()
        gd_client = gdata.spreadsheet.service.SpreadsheetsService()
        service = self.get_google_service()
        token = service._http.request.credentials.access_token
        gd_client.additional_headers = {'Authorization': 'Bearer %s' % token}

        try:
            for payload in payloads:
                try:
                    logging.debug("Pushing payload {}".format(payload))
                    gd_client.InsertRow(payload, FILE_ID, WORKSHEET_ID)
                except HTTPException as e:
                    logging.error("Re-Pushing {}".format(e.message))
                    gd_client.InsertRow(payload, FILE_ID, WORKSHEET_ID)
        except Exception as e:
            logging.error("Failed to push to Spreadsheet {}".format(e.message))

        # self.response.write("Here is the link {} {} {} ".format(file_id, worksheet_id, file_link))


    def get_payload(self):
        cur.execute("SELECT * FROM overstocks")
        columns = [str(i[0]) for i in cur.description]
        payload = []
        for row in cur:
            count = 0
            row_payload = {}
            for col in columns:
                row_payload[col.replace("_", "")] = str(row[count]).strip()
                count = count + 1
            payload.append(row_payload)
        return payload

    def get_google_service(self):
        from oauth2client.client import SignedJwtAssertionCredentials
        with file(os.path.join(os.path.dirname(__file__), config.service_acct_pem_file), "r") as f: SERVICE_ACC_KEY = f.read()
        _credentials = SignedJwtAssertionCredentials(config.service_acct_email, SERVICE_ACC_KEY, scope='https://www.googleapis.com/auth/drive')
        http = httplib2.Http(memcache)
        http = _credentials.authorize(http)
        service = discovery.build('drive', 'v2', http=http)
        return service

    def add_permission(self, service, permission, file_id):
        logging.debug("Push to {} {}".format(permission, file_id))
        return service.permissions().insert(fileId=file_id, sendNotificationEmails=True, body=permission).execute()

    def create_file(self):
        logging.info("Creating file")
        file_title = "CSV import"

        service = self.get_google_service()
        body = {
              'mimeType': 'application/vnd.google-apps.spreadsheet',
              'title': file_title,
            }
        file = service.files().insert(body=body).execute()
        file_id = file['id']
        file_link = file['alternateLink']
        logging.debug("File ID Created! {} {}".format(file_id, file_link))

        for shared_user in SPREADSHEET_SHARED:
            add_ok = False
            while not add_ok:
                try:
                    self.add_permission(service, {'value': shared_user, 'type': 'user', 'role': 'writer'}, file_id)
                except HTTPException as e:
                    add_ok = False
                    logging.warning("Permission writing error repush again {}".format(e.message))
                else:
                    add_ok = True

        # cur.execute("SELECT * FROM overstocks")
        columns = [i[0] for i in cur.description]
        # logging.debug(columns)

        gd_client = gdata.spreadsheet.service.SpreadsheetsService()
        token = service._http.request.credentials.access_token
        gd_client.additional_headers = {'Authorization': 'Bearer %s' % token, 'If-Match': "*"}
        wf = gd_client.GetWorksheetsFeed(file_id)
        default_worksheet = wf.entry[0]

        default_worksheet.col_count.text = str(len(columns))

        gd_client.UpdateWorksheet(default_worksheet)
        worksheet_id = default_worksheet.id.text.split('/')[-1]
        col = 1
        cells_feed = gd_client.GetCellsFeed(file_id, worksheet_id)
        cells_feed_link = cells_feed.GetSelfLink().href
        temp_batch_request = gdata.spreadsheet.SpreadsheetsCellsFeed()
        batch_request = copy.deepcopy(temp_batch_request)


        for header in columns:
            new_cell = gdata.spreadsheet.Cell(row=str(1), col=str(col), inputValue=header)
            batch_cell = gdata.spreadsheet.SpreadsheetsCell(cell=new_cell)
            temp_batch_request.AddUpdate(batch_cell)
            col += 1

        col = 1

        for entry in temp_batch_request.entry:
            entry_string = entry.ToString()
            entry_string = re.sub("<ns\d:id>(.+)</ns\d:id>", "<ns0:id>{}/R1C{}</ns0:id>".format(cells_feed_link, col), entry_string)
            new_entry = gdata.spreadsheet.SpreadsheetsCellFromString(entry_string)
            batch_request.AddUpdate(new_entry)
            col += 1

        gd_client.ExecuteBatch(batch_request, cells_feed.GetBatchLink().href, file_id, worksheet_id)


        return file_id, worksheet_id, file_link


application = webapp2.WSGIApplication([
    ('/', GoogleService),
], debug=True)
